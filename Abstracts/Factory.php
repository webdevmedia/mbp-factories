<?php # -*- coding: utf-8 -*-

namespace ModuleBasedPlugin\Factories\Abstracts;

use ModuleBasedPlugin\Factories\Interfaces;

abstract class Factory implements Interfaces\Factory {

	/**
	 * @var array
	 */
	private $entities = [];

    public function add(string $id, array $args) {
		$this->entities[ $id ] = $args;
	}

	/**
	 * @return array
	 */
	public function get(): array{
		return $this->entities;
	}

	/**
	 * @return array
	 */
	public function unset(string $id): array{
		unset($this->entities[ $id ]);
		return $this->entities;
	}

    public function initialize(string $entity, string $path, string $namespace, bool $is_static = true, array $arguments, array $exclude = []){
        $entityFolder = ucfirst($entity);

        $path = $path . '/' . $entityFolder;

        if(is_dir($path)){
            $tasks = $this->removeExcludes(glob($path.'/*.php'), $exclude);

            if(!empty($tasks)) {
                foreach ( $tasks as $task ) {
                    require_once( $task );
                    $id = str_replace('.php', '', basename($task));
                    $class =  empty($entityFolder) ? $namespace  . '\\' . $id : $namespace . '\\' . $entityFolder . '\\' . $id;

                    if($is_static === true ){
                        $args[$id] = $class;
                    }else {
	                    if(!empty($entity)) {
		                    $args = [ $id => (new $class($arguments))->$entity() ];
	                    }else{
		                    $args = [ $id => new $class($arguments) ];
	                    }
                    }

                    $this->add($entity, $args);
                }
            }
        }
	}	

    private function removeExcludes(array $files, array $exclude):array {
		$filtered = [];

	    if(!empty($files)){
		    foreach( $files as $file ) {
		    	if(!in_array(basename($file), $exclude)){
				    $filtered[] = $file;
			    }
	    	}
	    }

	    return $filtered;
    }
}