<?php # -*- coding: utf-8 -*-

namespace ModuleBasedPlugin\Factories\Abstracts;

abstract class Enqueue extends Factory{

	public static function validEnqueue(array $source){
		return !empty($source['src']) ?? true;
	}

	/**
	 * @param array  $items
	 * @param string $type - style | script
	 */
	public static function enqueue(array $items, string $type){
		$method = 'wp_enqueue_' . $type;

		if(!empty($items)) {
			foreach ( $items as $id => $item ) {
				$method(
					$id,
					$item[ 'src' ],
					!empty($item[ 'deps' ]) ? $item[ 'deps' ] : null,
					!empty($item[ 'version' ]) ? $item[ 'version' ] : null,
					!empty($item[ 'media' ]) ? $item[ 'media' ] : null
				);
			}
		}
	}


    public static function getScriptSuffix() {
	    return ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG === TRUE ) ? '' : '.min';
    }


    public static function gropeAssetsByType(array $assets, string $baseUrl, string $suffix): array{
	    $grouped = [];

        if(!empty($assets) && !empty($suffix)){
            foreach ($assets as $assetID => $asset ) {

                if(self::validEnqueue($asset)) {
                    $moduleBaseUrl = !empty($asset['url']) ? $asset['url'] : $baseUrl;

	                if ( file_exists( $baseUrl . $asset[ 'src' ] ) ) {

		                $baseUrlComponents = (array_merge(array_filter(explode(WP_CONTENT_DIR, $moduleBaseUrl))));
	                    $asset['src'] = WP_CONTENT_URL . $baseUrlComponents[0] . str_replace( '.' . $suffix, self::getScriptSuffix() . '.' . $suffix, $asset['src']);

		                $use = ! empty( $asset[ 'use@' ] ) ? $asset[ 'use@' ] : '';

		                switch ( $use ) {
			                case 'admin':
				                $grouped[ 'admin' ][ $assetID ] = $asset;
				                break;
			                case 'login':
				                $grouped[ 'login' ][ $assetID ] = $asset;
				                break;
			                case 'frontend':
			                default:
				                $grouped[ 'frontend' ][ $assetID ] = $asset;
				                break;
		                }

	                }
                }
            }
        }

        return $grouped;
    }

}
