<?php # -*- coding: utf-8 -*-

namespace ModuleBasedPlugin\Factories\Abstracts;

abstract class Entity extends Factory{

	/**
	 * Get all registered entities
	 *
	 * @return array
	 */
	public function getEntities(){
	    $entities = $this->get();
	    return $entities;
    }

	/**
	 * Remove all entities
	 */
	public function unsetEntities(){
	    $entities = $this->get();

	    if(!empty($entities)) {
		    foreach ($entities as $entity => $v) {
		        $this->unset($entity);
		    }
	    }
    }

	/**
	 * Load entities from module
	 *
	 * @param string $path
	 * @param string $fileType
	 * @param bool   $assoc
	 */
	public function loadEntities(string $path, string $fileType, bool $assoc = false){
		if(is_dir($path) && !empty($fileType)){
			$glob = glob($path.'/*.' . $fileType);

			if(!empty($glob)) {
				foreach ($glob as $config) {
					$id = str_replace( '.' . $fileType, '', basename($config));
					$content = [];

					if($fileType == 'json') {
						$content = json_decode( file_get_contents( $config ), ( ! empty( $assoc ) ? TRUE : FALSE ) );
					}else {
						$content[$id] = [$id];
					}

					try {
						if(!empty($content[ $id ])) {
							$this->add( $id, $content[$id] );
						}else{
							throw new \Exception("Invalid: §content[" . $id . "] does not match!");
						}

					} catch (\Exception $e) {
						echo '<pre>';print_r( [ 'Location' => [ 'PATH' => dirname( __FILE__ ), 'FILE' => basename( __FILE__ ), 'FUNCTION' => __FUNCTION__ . ':' . __LINE__ ], 'Exception' => [
							'msg' => $e->getMessage(),
							'$id' => $id,
							'$config' => $config,
							'$content' => $content
					    ]] );
						die();
					}

				}
			}
		}
	}

}
