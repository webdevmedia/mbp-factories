<?php # -*- coding: utf-8 -*-

namespace ModuleBasedPlugin\Factories\Abstracts;

abstract class Ajax extends Factory{

    public function wp_ajax(){
        $tasks = $this->get()['ajaxTasks'];

        if(!empty($tasks)) {
            foreach ($tasks as $name => $task){
                $action = str_replace('\\', '_', $task);
                add_action( 'wp_ajax_' . $action, function () use ($task, $name) {

	                try {
		                if(!class_exists($task)){
			                throw new \Exception("No class '$name' in '$task.php' exists!", 500);
		                }

		                if(!method_exists($task, 'doAction')){
			                throw new \Exception("$task.php has no static 'doAction()' method!", 500);
		                }
	                } catch (\Exception $e) {
		                echo '<pre>';print_r( ['Exception' => [
			                'msg' => $e->getMessage(),
			                'where' => __CLASS__ . ':' . __LINE__ ,
		                ]] );
		                die();
	                }

	                $task::doAction();
                });
            }
        }
    }

}
