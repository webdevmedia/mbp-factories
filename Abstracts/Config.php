<?php # -*- coding: utf-8 -*-

namespace ModuleBasedPlugin\Factories\Abstracts;

abstract class Config extends Factory{

	public $entities = [];

	public function get(): array {
		return parent::get();
	}

	public function getSetting(string $group = 'all', string $name = null) {
		$value = [];
		$settings = !empty($this->entities[$group]) ? $this->entities[$group] : ($group === 'all' ? $this->entities : null);

		if(!empty($settings)) {
			$value = $this->value( $name, $settings );

			if($group === 'all' && $name === null){
				$value = $settings;
			}
		}

		return $value;
	}

	public function setEntities(array $entities){
		$this->entities = !empty($entities) ? $entities : $this->entities;
	}

	public function add(string $group, $value): self {
	    $this->entities = $this->getSetting();

       if(empty($this->entities[$group])){
			$this->entities[$group] = $value;
		}else{
			if(is_array($value)){
				foreach($value as $k => $v){
					if(is_array($v)) {
						$this->entities[ $group ][ $k ] = array_merge( $v, $this->getSetting( $group, $k ) );
					}else{
						$this->entities[ $group ][ $k ] = $v;
					}
				}
			}else{
				$this->entities[$group][] = $value;
			}
		}

		return $this;
	}

	private function value(?string $name, array $settings) {
		return !empty($settings[$name]) ? $settings[$name] : $this->get();
	}

}
