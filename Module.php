<?php
namespace ModuleBasedPlugin\Factories;

use ModuleBasedPlugin\Factories\Interfaces,
	ModuleBasedPlugin\Handler;

/**
 * Class Moduless
 *
 * @package WordPressImprint\Factory
 */
class Module implements Interfaces\Module{

	/**
	 * @var array
	 */
	private static $modules = [];


	/**
	 * @var array
	 */
	private static $modulesinitialized = [];

	/**
	 * @var
	 */
	private static $modulePath;

    /**
     * @var
     */
    private static $factories;

	/**
	 * @var Handler\pluginConfig $pluginConfig
	 */
	private static $pluginConfig;

	/**
	 * @param Config $config
	 */
	public static function initModule(Handler\pluginConfig $pluginConfig){
		self::$pluginConfig = $pluginConfig;
		self::$factories = self::setFactories();
		self::$modules = self::scanModulesDir();

		foreach(self::$modules as $module => $moduleClasses){
			if(!empty($moduleClasses['moduleComponents'])) {
				foreach ($moduleClasses['moduleComponents'] as $moduleClass){
					$moduleNamespace = 'Module\\' . $module . '\\' . $moduleClass;
					$moduleName = $module;

					self::initializeModules($moduleNamespace, $moduleName, $moduleClass, $moduleClasses['moduleInfo']);
				}
			}
		}

		do_action(__NAMESPACE__ . '\\ModulesInitialize' );
	}


	public static function initializeModules(string $moduleNamespace, string $moduleName, string $moduleClass, array $moduleInfo): array{
 	    $moduleFactoryName = "\\" . self::$pluginConfig->getSetting( 'plugin', 'namespace') . "\\" . $moduleNamespace;

        $class = new \ReflectionClass($moduleFactoryName);

		if($class->implementsInterface(__NAMESPACE__ . '\Interfaces\Factory')){
			$moduleInfo['namespace____'] = $moduleNamespace;
			self::$pluginConfig->add($moduleName, $moduleInfo);

			$moduleFactory = new $moduleFactoryName(self::$pluginConfig);
			self::addModule($moduleName, $moduleFactory);
        }

		return self::$modulesinitialized;
	}

	private static function addModule($moduleName, $module){

		foreach(self::$factories as $factory){
			$moduleName = str_replace('\\' . $factory, '', $moduleName );
		}

		self::$modulesinitialized[$moduleName] = $module;
	}

	/**
	 * @return array
	 */
	public static function getModules(): array{
		return self::$modulesinitialized;
	}

    /**
     * @param string $modulePath
     *
     * @return string
     */
    public static function setModulesPath(string $modulePath = '' ) {
		self::$modulePath = (!empty($modulePath)) ? $modulePath : self::$pluginConfig->getSetting( 'plugin', 'baseDir') . 'inc/Module';
		return self::$modulePath;
	}

	/**
	 * @return array
	 */
	private static function scanModulesDir(){
		$modules = [];
		$modulesPath = (empty(self::$modulePath) ? self::setModulesPath() : self::$modulePath );
		$moduleContent = scandir($modulesPath);

		foreach($moduleContent as $module){
			$modulePath = $modulesPath . '/' . $module;

			if(is_dir($modulePath) && $module != '.' && $module != '..' ){
			    
			    foreach(self::$factories as $factory){
                    if( file_exists($modulePath.'/' . $factory . '.php')){
	                    $modules[$module]['moduleInfo']['name'] = $module;
	                    $modules[$module]['moduleInfo']['path'] = $modulePath . '/';
	                    $modules[$module]['moduleComponents'][] = $factory;
                    }
                }
			}
		}

		return $modules;
	}

    private static function setFactories(): array {
        $factories = [];
        $factoriesPath = (!empty($modulePath)) ? $modulePath : self::$pluginConfig->getSetting( 'plugin', 'baseDir') . 'inc/Factories';

        if(is_dir($factoriesPath)){
            $glob = glob($factoriesPath.'/Abstracts/*.php');

            foreach($glob as $factory){
                $factories[] = str_replace('.php', '', basename($factory));
            }

        }

        return $factories;
    }

}
