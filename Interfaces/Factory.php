<?php # -*- coding: utf-8 -*-

namespace  ModuleBasedPlugin\Factories\Interfaces;

interface Factory{

	public function add(string $id, array $args);

	public function get(): array;

	public function unset(string $id): array;

}
