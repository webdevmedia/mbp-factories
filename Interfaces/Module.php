<?php # -*- coding: utf-8 -*-

namespace ModuleBasedPlugin\Factories\Interfaces;

interface Module{

	public static function initializeModules(string $moduleNamespace, string $moduleName, string $moduleClass, array $moduleInfo): array;

	/**
	 * @return array
	 */
	public static function getModules(): array;

	/**
	 * @param mixed $modulePath
	 */
	public static function setModulesPath(String $modulePath = '' );

}
