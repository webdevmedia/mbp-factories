<?php # -*- coding: utf-8 -*-

namespace  ModuleBasedPlugin\Factories\Interfaces;

interface Tasks{

	public function tasks();

	public function prepareTask();

}
